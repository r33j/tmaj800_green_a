cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-statusbar.statusbar",
      "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
      "pluginId": "cordova-plugin-statusbar",
      "clobbers": [
        "window.StatusBar"
      ]
    },
    {
      "id": "cordova-plugin-navigationbar-color.navigationbar",
      "file": "plugins/cordova-plugin-navigationbar-color/www/navigationbar.js",
      "pluginId": "cordova-plugin-navigationbar-color",
      "clobbers": [
        "window.NavigationBar"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-plugin-statusbar": "2.4.3",
    "cordova-custom-config": "5.1.0",
    "cordova-plugin-navigationbar-color": "0.0.8"
  };
});