import Vue from 'vue';
import App from './App.vue';


Vue.config.productionTip = false;
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { store } from './data/store';
import 'vue-sequential-entrance/vue-sequential-entrance.css';
import AOS from 'aos';
import 'aos/dist/aos.css';
import VueOffline from 'vue-offline';

Vue.use(VueOffline);



import SequentialEntrance from 'vue-sequential-entrance';
Vue.use(SequentialEntrance);

Vue.use(AOS);

Vue.use(Vuex);



import routes from './routes';

Vue.config.productionTip = false;
const router = new VueRouter({mode: 'history', routes});


Vue.use(VueRouter);


new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');