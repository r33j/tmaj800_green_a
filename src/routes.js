import Register from './components/Register.vue';
import Login from './components/Login.vue';
import Dashboard from "./components/Dashboard";
import Settings from "./components/Settings.vue"
import Chat from "./components/Chat.vue"
import UserInfo from "./components/UserInfo.vue";
import Products from "./components/Products.vue";
import ForgotPassword from "./components/ForgotPassword";

const routes = [
    { path: '/', component: Login },
    { path: '/register', component: Register },
    { path: '/forgot_password', component: ForgotPassword },
    { path: '/dashboard', 
        component: Dashboard,
        children: [{
            name: 'dashboardRoute',
            path: '',
            component: Dashboard
        },
        {
            name: 'dashboardRoute.products',
            path: 'products',
            component: Products
        },
        {
            name: 'dashboardRoute.chat',
            path: 'chat',
            component: Chat
        },
        {
            name: 'dashboardRoute.settings',
            path: 'settings',
            component: Settings
        },
        {
            name: 'dashboardRoute.userInfo',
            path: 'userInfo',
            component: UserInfo
        }
    
    ]}
];

export default routes;